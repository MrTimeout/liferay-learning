package org.learning.say.hello.world.portlet;

import java.util.Objects;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.learning.say.hello.world.constants.HelloWorldPorletKeys;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;

@Component(immediate = true, property = {
    "javax.portlet.name=" + HelloWorldPorletKeys.HELLO_WORLD,
    "mvc.command.name=action-hello"
}, service = MVCActionCommand.class)
public class HelloWorldMVCActionCommand implements MVCActionCommand {

  private static final Log LOGGER = LogFactoryUtil.getLog(HelloWorldMVCActionCommand.class);

  @Override
  public boolean processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {
    final String personName = ParamUtil.getString(actionRequest, "name");
    LOGGER.info("Hello " + personName);
    return Objects.nonNull(personName);
  }

}
