package org.learning.say.hello.world.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import static org.learning.say.hello.world.constants.HelloWorldPorletKeys.HELLO_WORLD;
import static org.learning.say.hello.world.constants.HelloWorldPorletKeys.HELLO_WORLD_RENDER_COMMAND_EDIT_FILE;

import org.osgi.service.component.annotations.Component;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

@Component(immediate = true, property = {
    "javax.portlet.name=" + HELLO_WORLD, // This is the name of the portlet in the liferay portal
    "mvc.command.name=edit-hello" // unique name to identify this
}, service = MVCRenderCommand.class)
public class HelloWorldMVCRenderCommand implements MVCRenderCommand {

  @Override
  public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
    return HELLO_WORLD_RENDER_COMMAND_EDIT_FILE;
  }

}
