package org.learning.say.hello.world.constants;

public class HelloWorldPorletKeys {

  public static final String HELLO_WORLD = "HelloWorldPorlet";

  public static final String HELLO_WORLD_RENDER_COMMAND_EDIT_FILE = "/edit.jsp";
}
