<%@ include file="/init.jsp" %>
<liferay-ui:header title="Edition mode" backURL="<%= backURL %>"></liferay-ui:header>

<h1><liferay-ui:message key="edit.title"/></h1>

<liferay-portlet:actionURL var="updateURL" name="action-hello"></liferay-portlet:actionURL>

<aui:form action="<%= updateURL %>">
  <aui:input name="name" label="Name of the person" type="text"></aui:input>

  <aui:button-row>
    <aui:button type="submit" value="save"></aui:button>
    <aui:button type="cancel" value="cancel" href="<%= backURL %>"></aui:button>
  </aui:button-row>
</aui:form>