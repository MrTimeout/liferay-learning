<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState" %>
<%@ include file="/init.jsp" %>

<p>
	<b><liferay-ui:message key="helloworld.caption"/></b>
	<br />
	<liferay-ui:message key="byeworld.caption"/>

	<liferay-portlet:renderURL var="editURL">
	  <liferay-portlet:param name="mvcPath" value="/edit.jsp"/>
		<liferay-portlet:param name="backURL" value="<%= currentURL %>"/>
	</liferay-portlet:renderURL>

	<liferay-portlet:renderURL var="editURLRightWay">
		<liferay-portlet:param name="mvcRenderCommandName" value="edit-hello"/>
		<liferay-portlet:param name="backURL" value="<%= currentURL %>"/>
	</liferay-portlet:renderURL>

	<%-- 
		Here we have 4 types of LiferayWindowState:
			- MAXIMIZED
			- MINIMIZED
			- POP_US
			- EXCLUSIVE
	--%>
	<liferay-portlet:renderURL var="editURLRigthWayWithCustomWindow" windowState="<%= LiferayWindowState.MAXIMIZED.toString() %>">
		<liferay-portlet:param name="mvcRenderCommandName" value="edit-hello" />
		<liferay-portlet:param name="backURL" value="<%= currentURL %>"/>
	</liferay-portlet:renderURL>

	<br />
	<aui:a href="<%= editURL %>">Edit</aui:a>
	<br />
	<aui:a href="<%= editURLRightWay %>">Edit right way</aui:a>
	<br />
	<aui:a href="<%= editURLRigthWayWithCustomWindow %>">Edit right way with custom window</aui:a>
</p>