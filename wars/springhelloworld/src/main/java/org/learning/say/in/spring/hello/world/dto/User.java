package org.learning.say.in.spring.hello.world.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

/**
 * @author mrtimeout
 */
public class User implements Serializable {

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	private static final long serialVersionUID = 1113488483222411111L;

	@NotBlank
	private String firstName;

	@NotBlank
	private String lastName;

}