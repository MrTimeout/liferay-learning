# Learning liferay

## Project structure

[Best link](https://help.liferay.com/hc/en-us/articles/360017885352-Configuring-a-Liferay-Workspace-)

## Blade CLI

[Creating-your-first-project](https://help.liferay.com/hc/en-us/articles/360017885252-Creating-a-Liferay-Workspace-with-Blade-CLI-)

## Cheatsheet

- `blade init -v 7.1 -b maven [workspace-name]`: It is used to generate a liferay workspace.
  + -v/--liferay-version [7.0|7.1|7.2|7.3]
  + -b/--build [maven|gradle]
- `blade server init -b maven`: It is used to accomplish the 'InitBundle' phase, where blade downloads the tar.gz tomcat (which is specified in the maven property `liferay.workspace.bundle.url`)
- `blade server start -b`: it is used to start the liferay server
  + -d is used to deploy in debug mode.
- `blade create --base ~/gitlab/liferay-workspace/modules --build maven --classname HelloWorld --liferay-version 7.1 --package-name org.learning.say.hello.world --template mvc-portlet [project-name]`
  + `blade help create`
  + `blade create --list-templates/-l`
  + `liferay.workspace.modules.dir=modules` is used in gradle.properties to set the module dir to store modules.
- `blade deploy --watch --build maven --trace`: It is used to deploy a module or the entire project.

### MVC Portlet Module

```sh
blade create --base ~/gitlab/liferay-workspace/modules \
  --build maven \
  --classname HelloWorld \
  --liferay-version 7.1 \
  --package-name org.learning.say.hello.world \
  --template mvc-portlet [project-name]
```

### Spring MVC in portlet

```sh
# --framework (springportletmvc|portletmvc4spring)
# --view-type (jsp|thymeleaf)
blade create --base ~/gitlab/liferay-workspace/modules/ \
  --build maven \
  --classname SpringHelloWorld \
  --liferay-version 7.1 \
  --package-name org.learning.say.in.spring.hello.world \
  --template spring-mvc-portlet \
  --framework springportletmvc \
  --view-type jsp \
  springhelloworld
```
